// rollup.config.js
import fs from 'fs';
import path from 'path';
import vue from 'rollup-plugin-vue';
import alias from '@rollup/plugin-alias';
import commonjs from '@rollup/plugin-commonjs';
import replace from '@rollup/plugin-replace';
import babel from 'rollup-plugin-babel';
import { terser } from 'rollup-plugin-terser';
import minimist from 'minimist';
import nodeResolve from "rollup-plugin-node-resolve";
// import vue from "./node_modules/rollup-plugin-ts-vue/dist/rollup-plugin-ts-vue.es";
import postcss from 'rollup-plugin-postcss'
import css from 'rollup-plugin-css-only'
// Get browserslist config and remove ie from es build targets
const esbrowserslist = fs.readFileSync('./.browserslistrc')
  .toString()
  .split('\n')
  .filter((entry) => entry && entry.substring(0, 2) !== 'ie');

const argv = minimist(process.argv.slice(2));

const projectRoot = path.resolve(__dirname);
const baseConfig = {
  input: './entry.ts',
  plugins: {
    preVue: [
      alias({
        resolve: ['.js', '.jsx', '.ts', '.tsx', '.vue'],
        entries: {
          '@': path.resolve(projectRoot, 'components'),
        },
      }),
    ],
    replace: {
      'process.env.NODE_ENV': JSON.stringify('production'),
      'process.env.ES_BUILD': JSON.stringify('false'),
    },
    vue: {
      css: true,
      template: {
        isProduction: true,
      },
    },
    babel: {
      exclude: 'node_modules/**',
      extensions: ['.js', '.jsx', '.ts', '.tsx', '.vue'],
      babelrc: false,
      runtimeHelpers: true,
      externalHelpers: true,
      plugins: [
        "@babel/plugin-transform-async-to-generator",
        "@babel/plugin-transform-regenerator",
        [
          "@babel/plugin-transform-runtime", {
            helpers: true,
            regenerator: true
          }
        ],
        [
          "@babel/plugin-proposal-decorators",
          {
            legacy: true
          }
        ],
      ],
      presets: [
        "@babel/preset-env",
        "@babel/preset-typescript",
      ],
      exclude: "node_modules/**"
    },
    nodeResolve: {
      mainFields: [  'module', 'main', 'jsnext', 'jsnext:main']
    }
  },
};

// ESM/UMD/IIFE shared settings: externals
// Refer to https://rollupjs.org/guide/en/#warning-treating-module-as-external-dependency
const external = [
  // list external dependencies, exactly the way it is written in the import statement.
  // eg. 'jquery'
  'vue',
  'vuetify',
  'axios',
];

// UMD/IIFE shared settings: output.globals
// Refer to https://rollupjs.org/guide/en#output-globals for details
const globals = {
  // Provide global variable names to replace your external imports
  // eg. jquery: '$'
  vue: 'Vue',
  axios: 'axios',
  vuetify: 'Vuetify',
  // "vue-property-decorator": "VueClassComponent",
  // "vue-class-component": "VueClassComponent",
  "@babel/runtime/regenerator": "regeneratorRuntime",
  '@babel/runtime/helpers/slicedToArray': 'slicedToArray',
  '@babel/runtime/helpers/asyncToGenerator': 'asyncToGenerator',
};

// Customize configs for individual targets
const buildFormats = [];
if (!argv.format || argv.format === 'es') {
  const esConfig = {
    ...baseConfig,
    external,
    output: {
      file: 'dist/vfrxt.esm.js',
      format: 'esm',
      exports: 'named',
    },
    plugins: [
      replace({
        ...baseConfig.plugins.replace,
        'process.env.ES_BUILD': JSON.stringify('true'),
      }),
      ...baseConfig.plugins.preVue,
      css(),
      vue(
        baseConfig.plugins.vue
      ),
      postcss(),
      babel({
        ...baseConfig.plugins.babel,
        presets: [
          [
            '@babel/preset-env',
            {
              targets: esbrowserslist,
            },
          ],
        ],
      }),
      nodeResolve({
        ...baseConfig.plugins.nodeResolve
      }),
      commonjs(),
    ],
  };
  buildFormats.push(esConfig);
}

if (!argv.format || argv.format === 'cjs') {
  const umdConfig = {
    ...baseConfig,
    external,
    output: {
      compact: true,
      file: 'dist/vfrxt.ssr.js',
      format: 'cjs',
      name: 'VFrxt',
      exports: 'named',
      globals,
    },
    plugins: [
      replace(baseConfig.plugins.replace),
      ...baseConfig.plugins.preVue,
      css(),
      vue({
        ...baseConfig.plugins.vue,
        template: {
          ...baseConfig.plugins.vue.template,
          optimizeSSR: true,
        },
      }),
      postcss(),
      babel(baseConfig.plugins.babel),
      nodeResolve({
        ...baseConfig.plugins.nodeResolve
      }),
      commonjs(),
    ],
  };
  buildFormats.push(umdConfig);
}

if (!argv.format || argv.format === 'iife') {
  const unpkgConfig = {
    ...baseConfig,
    external,
    output: {
      compact: true,
      file: 'dist/vfrxt.min.js',
      format: 'iife',
      name: 'VFrxt',
      exports: 'named',
      globals,
    },
    plugins: [
      replace(baseConfig.plugins.replace),
      ...baseConfig.plugins.preVue,
      css(),
      vue(
        baseConfig.plugins.vue
      ),
      postcss(),
      babel(baseConfig.plugins.babel),
      nodeResolve({
        ...baseConfig.plugins.nodeResolve
      }),
      commonjs(),
      terser({
        output: {
          ecma: 5,
        },
      }),
    ],
  };
  buildFormats.push(unpkgConfig);
}

if (!argv.format || argv.format === 'umd') {
  const umdConfig = {
    ...baseConfig,
    external,
    output: {
      file: 'dist/vfrxt.umd.js',
      format: 'umd',
      name: 'VFrxt',
      exports: 'named',
      globals,
    },
    plugins: [
      replace({
        ...baseConfig.plugins.replace,
        'process.env.UMD_BUILD': JSON.stringify('true'),
      }),
      ...baseConfig.plugins.preVue,
      css(),
      vue(
        baseConfig.plugins.vue
      ),
      postcss(),
      babel({
        ...baseConfig.plugins.babel,
        presets: [
          [
            '@babel/preset-env',
            {
              targets: esbrowserslist,
            },
          ],
        ],
      }),
      nodeResolve({
        ...baseConfig.plugins.nodeResolve
      }),
      commonjs({
        // namedExports: {
        //   './components/VFrxt.vue': ['VFrxt' ] ,
        // },
      }),
    ],
  };
  buildFormats.push(umdConfig);
}

// Export config
export default buildFormats;
