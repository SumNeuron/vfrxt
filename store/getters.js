const getters = {
  hasNoCards: (state) => {
    return state.cards === null
  },
  cardIds: (state, getters) => {
    if (getters.hasNoCards) return []
    return Object.keys(state.cards)
  },
  getCard: (state) => (name) => {
    return state.cards[name]
  }
}

export default getters
