import Vue, { PluginFunction, VueConstructor } from 'vue';


interface InstallFunction extends PluginFunction<any> {
  installed?: boolean;
}

declare const VFrxt: { install: InstallFunction };
export default VFrxt;

export const VCnf: VueConstructor<Vue>;
export const VFrxtInput: VueConstructor<Vue>;
export const VRecordsTable: VueConstructor<Vue>;
export const VTimSort: VueConstructor<Vue>;
