const state = () => ({
  cards: null,

  isFetching: false, // is fetching with scryfall
  cardsFound: 0,
  totalCards: Infinity,

  tooltipData: null,

  snackModel: false,
  snackText: null,
})


export default state
