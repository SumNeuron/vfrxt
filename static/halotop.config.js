import {configs} from 'frxt'

export const cnfConditionalConfig = Object.assign({}, configs.defaultCNFConditionalConfig)
let c = (a, b) => {
      let includes = false
      if (Array.isArray(a)) {
        for (let i = 0; i < a.length; i++) {
          if (a[i] == b) {
            includes = true
          }
          else if (typeof b === 'string') {
            includes = a[i].toLowerCase().includes(b.toLowerCase())
          }
          if (includes) {
            return includes
          }
        }
      } else if (typeof a === 'string') {
        return a.includes(b)
      } else { return includes }
      return includes
    }

cnfConditionalConfig.ss.conditional = c
cnfConditionalConfig.nss.conditional = (a, b) => {
  let includes = true
  if (Array.isArray(a)) {
    if (typeof b !== 'string')
    return a.every(e => e != b)
    return a.every(e => {
      return !e.toLowerCase().includes(b.toLowerCase())
    })
  } else if (typeof a === 'string') {
    return !a.includes(b)
  }
  return includes
}

export const fieldSynonymMap = {
  'Flavor': 'name'
  // 'name': 'Flavor'
}

export const fieldRenderFunctions = {
  Calories: (i, f, r) => {
    return `${r[f].container.calories} per tub`
  },
  Ingredients: (i, f, r) => {
    return `${r[f].length} Ingredients (${r[f][0]}, ...)`
  },
  Protein: (i, f, r) => {
    let c = r[f].container.amount
    return `${c} per tub`
  },
  'Total Fat': (i, f, r) => {
    let c = r[f].container.amount
    return `${c} per tub`
  },
  'Total Sugars': (i, f, r) => {
    let c = r[f].container.amount
    return `${c} per tub`
  },
  'Total Carbohydrate': (i, f, r) => {
    let c = r[f].container.amount
    return `${c} per tub`
  },
}

export const fieldSortByFunctions = {
  Ingredients: (i, f, r) => {
    return r[f].length
  }
}

export const fieldFilterFunctions = {
  name: (i, f, r) => {
    return r[f].toLowerCase()
  }
}

export const makeNutritionLabelFn = (
  {mode='render', which='container'}
) => {
  return (i, f, r) => {
    let v = r[f][which].amount
    if (mode !== 'render') {
      let n = v.replace('g', '').trim()
      if (isNaN(n)) return n
      return Number(n)
    }
    return `${v} per ${which==='container' ? 'tub' : which}`
  }
}

export const makeCaloriesLabelFn = (
  {mode='render', which='container'}
) => {
  return (i, f, r) => {
    let v = r[f][which].calories

    if (mode !== 'render') {
      let n = v.slice(v.lenth-1)
      if (isNaN(n)) return n
      return Number(n)
    }
    return `${v}kCal per ${which==='container' ? 'tub' : which}`
  }
}

const extractorConfig = {}
let keys = ['Protein', 'Total Fat', 'Total Sugars', 'Total Carbohydrate', 'Calories']
keys.forEach((k)=>{
  let o = {}
  let subs = ['serving', 'container']
  let modes = ['render', 'filter', 'sortBy']
  subs.forEach((s)=>{
    let c = {}
    modes.forEach((m)=>{
      c[`${m}Config`] = {
        functionName: k !== 'Calories' ? 'makeNutritionLabelFn' : 'makeCaloriesLabelFn',
        params: {mode:m, which:s}
      }
    })
    o[s] = c
  })
  extractorConfig[k] = o
})

const functionLookup = {
  makeNutritionLabelFn, makeCaloriesLabelFn
}

export const fieldOrder = [
      'name', 'Calories', 'Ingredients',
      'Protein',
      'Total Fat', 'Total Sugars', 'Total Carbohydrate'
    ]

export {extractorConfig, functionLookup}

export const tknConditionalMap = {
  eq: 'eq',
  is: 'eq',
  equal: 'eq',
  '=': 'eq',
  '!=': 'neq',
  '≠': 'neq',
  '>': 'gt',
  '≥': 'gte',
  '>=': 'gte',
  '<': 'lt',
  '≤': 'lte',
  '<=': 'lte',
  'is not': 'neq',
  neq: 'neq',
  'not equal to': 'neq',
  gt: 'gt',
  'greater than': 'gt',
  'less than': 'lt',
  lt: 'lt',
  'less than or equal to': 'lte',
  'greater than or equal to': 'gte',
  'member of': 'ss',
  substring: 'ss',
  contains: 'ss',
  includes: 'ss',
  has: 'ss',
  '!has': 'nss',
  'does not contain': 'nss',
  'does not include': 'nss',
  'does not have': 'nss',
  'not contain': 'nss',
  'not include': 'nss',
  '!contains': 'nss',
  '!includes': 'nss'
}
