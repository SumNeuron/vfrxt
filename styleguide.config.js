const { resolve, join } = require('path')
const { VueLoaderPlugin } = require('vue-loader')

const pkg = require('./package.json')
const docSiteUrl = process.env.DEPLOY_PRIME_URL || 'https://sumneuron.gitlab.io/vfrxt'


module.exports = {
	components: `./components/**/[A-Z]*.vue`,
	template: {
		head: {
			links: [
				{
					href: 'https://fonts.googleapis.com/css?family=Roboto:300,400,500,700|Material+Icons',
					rel: 'stylesheet'
				},
				{
          rel: 'stylesheet',
          href:
            'https://fonts.googleapis.com/css?family=Material+Icons',
        },
				{
					rel: 'stylesheet',
					href: 'https://cdn.jsdelivr.net/npm/@mdi/font@4.x/css/materialdesignicons.min.css'
				}
			]
		}
	},
	require: [
		resolve(__dirname, 'styleguide/global.requires.js'),
    resolve(__dirname, 'styleguide/global.styles.scss')
  ],
	renderRootJsx: resolve(__dirname, 'styleguide/styleguide.root.js'),
	validExtends: fullFilePath => !/(?=node_modules)(?!node_modules\/vuetify)/.test(fullFilePath),
	ribbon: {
		text: 'Back to examples',
		url: `${docSiteUrl}`
	},
	webpackConfig: {
		module: {
			rules: [
				{
	        test: /\.tsx?$/,
	        use: [
	          {
	            loader: 'ts-loader',
	            options: {
	              transpileOnly: true
	            }
	          }
	        ]
	      },
				{
					test: /\.vue$/,
					loader: 'vue-loader'
				},
				{
					test: /\.js$/,
					exclude: /node_modules/,
					use: {
						loader: 'babel-loader',
						options: require('./babel.config')
					}
				},
				{
					test: /\.css$/,
					use: ['vue-style-loader', 'css-loader']
				},
				{
					test: /\.s(c|a)ss$/,
					use: [
						'vue-style-loader',
						'css-loader',
						{
							loader: 'sass-loader',
							options: {
								implementation: require('sass'),
								sassOptions: {
									fiber: require('fibers')
									// indentedSyntax: true // optional
								}
							}
						}
					]
				}
			]
		},
		plugins: [new VueLoaderPlugin()]
	},
	usageMode: 'expand',
	styleguideDir: 'pages/docs'
}
