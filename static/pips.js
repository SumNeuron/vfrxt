import PipMap from '@/static/PipMap.json'
let base = process.env.isPages ? '/vfrxt' : ''

export const manaPipToURL = (pip) => `${base}/pips/${PipMap[pip]}`

export const manaPipToHTML = (pip) => {
  let url = manaPipToURL(pip)
  return `
  <abbr style="${PIP_STYLE}background-image: url('${url}')">
    ${pip}
  </abbr>
  `
}

export const PIP_STYLE = `
display: inline-block;
margin: 1px 1px -1px 1px;
border-radius: 500px;
box-shadow: -1px -1px 0 rgba(0,0,0,0.85);
text-indent: -999em;
overflow: hidden;
width: 15px;
height: 15px;
background-size: 100% 100%;
background-position: top left;
`
